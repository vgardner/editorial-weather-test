<?php

namespace Drupal\custom_weather\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'Weather' Block.
 *
 * @Block(
 *   id = "weather_block",
 *   admin_label = @Translation("Weather Block"),
 *   category = @Translation("Weather Block"),
 * )
 */
class WeatherBlock extends BlockBase {
  /**
   * {@inheritdoc}
   */
  public function build() {

    $weatherService = \Drupal::service('custom_weather.custom_services');

    // User the content's postcode if the field exists.
    $node = \Drupal::routeMatch()->getParameter('node');
    if ($node instanceof \Drupal\node\NodeInterface && $node->hasField('field_postcode')) {
      $postcode_field_value = $node->get('field_postcode')->getValue()[0]['value'];

      if (!empty($postcode_field_value)) {
        $weatherService->setPostcode($postcode_field_value);
      }
    }

    return array(
      '#markup' => $this->t($weatherService->getCurrentWeather()),
      '#cache' => array('max-age' => 0),
    );
  }
}