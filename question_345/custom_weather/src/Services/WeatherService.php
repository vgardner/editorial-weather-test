<?php

namespace Drupal\custom_weather\Services;

use Drupal\custom_weather\OpenWeatherMap;

/**
 * Class CustomService.
 */
class WeatherService {

  public $postcode;

  /**
   * Constructs a new WeatherService object.
   */
  public function __construct() {
    $this->postcode = '10001';
  }


  /**
   * Sets the postcode for the weather service.
   *
   * @param $postcode string
   *  The postcode of the location to retrieve weather. Only US postcodes for now.
   */
  public function setPostcode($postcode) {
    $this->postcode = $postcode;
  }

  /**
   * Gets the current postcode.
   *
   * @return string
   */
  public function getPostcode() {
    return $this->postcode;
  }

  /**
   * Fetches the data from OpenWeatherMap.
   *
   * @return string
   *  Return values are 'rainy, snowy, cloudy, sunny'
   */
  public function fetchServiceData() {
    
    $cid = 'custom_weather:' . $this->postcode;
    // Get cached weather forecast if it exists.
    if ($cache = \Drupal::cache()->get($cid)) {
      $weather_forecast = $cache->data;
    }
    else {
      $openweathermap = new OpenWeatherMap();
      $weather_forecast = $openweathermap->getSimplifiedWeather($this->postcode);
      // Set cache for a specific postcode. Expires after one hour.
      \Drupal::cache()->set($cid, $weather_forecast, REQUEST_TIME + (3600));
    }

    return $weather_forecast;
  }


  /**
   * Returns the current weather.
   *
   * @return string
   */
  public function getCurrentWeather() {
    $weather = $this->fetchServiceData();
    return $weather;
  }
}