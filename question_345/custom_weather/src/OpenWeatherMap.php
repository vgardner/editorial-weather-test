<?php
namespace Drupal\custom_weather;

class OpenWeatherMap {
  private $appid;
  private $endpoint;

  /**
   * Constructs a new OpenWeatherMap object.
  */
  public function __construct() {
    // The API key and endpoint should be configurable. 
    // Included in code only for test purposes.
    $this->appid = 'c724080da16255aa67289d516794bbe4';
    $this->endpoint = 'https://api.openweathermap.org/data/2.5/weather';
  }

  /**
   * Returns the weather data from OpenWeatherMap based on a postcode.
   *
   * @param $postcode string
   *  The postcode of the location to retrieve weather. Only US postcodes for now.
   *
   * @return array
   */
  public function fetchWeatherData($postcode) {
    $client = new \GuzzleHttp\Client();
    try {
      $request = $client->get($this->endpoint, [
        'query' => [
          'appid' => $this->appid,
          'zip' => $postcode . ',us',
        ]
      ]);

      return json_decode($request->getBody());
    }
    catch (\Exception $e) {
      return NULL;
    }
  }


  /**
   * Returns a simplified version of the weather.
   *
   * @param $postcode string
   *  The postcode of the location to retrieve weather. Only US postcodes for now.
   *
   * @return string
   *  Return values are 'rainy, snowy, cloudy, sunny'
   */
  public function getSimplifiedWeather($postcode) {
    $weather_data = $this->fetchWeatherData($postcode);
    if (empty($weather_data)) {
      return 'Could not find forecast for this location.';
    }

    // IDs are based on https://openweathermap.org/weather-conditions
    $weather_id = $weather_data->weather[0]->id;
    $weather_forecast = "sunny";
    if ($weather_id < 600) {
      $weather_forecast = 'rainy';
    }
    elseif ($weather_id >= 600 && $weather_id < 700) {
      $weather_forecast = 'snowy';
    }
    elseif (($weather_id >= 700 && $weather_id < 800) || $weather_id > 800) {
      $weather_forecast = 'cloudy';
    }
    elseif ($weather_id == 800) {
      $weather_forecast = 'sunny';
    }

    return $weather_forecast;
  }
}