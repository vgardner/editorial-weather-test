# Editorial / Weather test

### All exercises were answered and tested using Drupal 8.6.

## Question 1
For this scenario, I’d use the Paragraphs module to allow editors to reorder fields when editing or creating an Article. For this, 3 Paragraph types would have to be created for each field and the Paragraph field added to the Article content type.

run ``composer require 'drupal/paragraphs:^1.3'``

``drush en paragraphs (or with Drupal Console - drupal module:install paragraphs)``

As an admin, go to Structure -> Paragraph Types ->  Add Paragraph Type
1 . Create a Paragraph called “Call To Action” and should contain field called Link of the type Link.

2. Create a Paragraph called “WYSIWYG” and should contain a field called “WYSIWYG” of the type “Text (formatted, long)”

3. Create a Paragraph called “Image” and should contain a field called Image with the name “Image”.

4. Go to Structure -> “Manage fields” on the Article content type -> Add a field of the type Paragraph. Under “Reference type, select “Include the selected below” and select the 3 paragraph types created.

This will allow editors to add all 3 fields multiple times and reorder them as they wish.

Exported configuration (from a new Drupal site) can be found at https://gitlab.com/vgardner/editorial-weather-test/tree/master/question_1

## Question 2
I’ve created a custom module that implements the hook form_alter with a simple solution for this specific scenario. I’m making the assumption the “administrator” here is the standard admin role that has access to ALL the permissions, hence the solution in code rather than in Drupal’s permission panel. In a scenario where other roles are created, such as a “production” role and a “editor” role, and “production” (for example) wouldn’t be allowed to view this description on the title field, I’d create a custom permission for this and give only access to the “editor” role.

Module with code can be found under https://gitlab.com/vgardner/editorial-weather-test/tree/master/question_2

## Question 3
Since the client doesn’t yet know where the weather information will be displayed, I’ve created a Service that will fetch the weather from OpenWeatherMap.org. This service can then be easily implemented in any other component.

Code for questions 3,4 and 5 can be found at: https://gitlab.com/vgardner/editorial-weather-test/tree/master/question_345

Caveats:
- I've included the value "snowy" since the API returns ids for a snowy weather as well).
- OpenWeatherMap was not returning the weather based on UK postcodes, so it's currently set and test with US codes. (I also had trouble finding a free and decent API to use UK postcodes, I hope this isn't an issue as the exercise specifies London)
- In case a location isn't found, the value "Could not find forecast for this location." is displayed.


## Question 4
For this scenario, I created a text field called Postcode and iemplmented a custom Block called Weather Block that consumes the OpenWeatherMap service. The block uses the value from the Postcode, if it exists in the content, or uses a default postcode if not.
- To place the Postcode field under the Title, go to Structure -> Article -> Manage fields -> "Manage form display" and drag the newly created Postcode field under the title field. And in "Manage display" drag the Postcode field to the top of the list.
- To place the Weather Block, install the custom_weather module. Then go to Structure -> Block Layout -> and under Content click on "Place Block". Select the Weather Block module and include it at the bottom of the Content section.

## Question 5
Custom blocks get cached by default. My solution here was to disable the standard caching of the custom block by returning "'#cache' => array('max-age' => 0)," in its build() method. I then implemented a custom caching mechanism in the Weather Service based on the postcode. This is using Drupal's Cache API and expires every 1 hour to cater for sudden changes in the forecast.


## A few caveats about the whole exercise
- I have not included tests due to time spent on this task, but I would prefer to include them in a production scenario.
- OpenWeatherMap has a PHP library which I did not use to demonstrate some code.
- There are hardcoded values such as the API key and the endpoint, which I would make configurable in a production scenario by adding an admin panel.
- The solutions presented here are the simplest I could think of based on the information provided and would probably differ on a production to cater for more scenarios or make the code more reusable.